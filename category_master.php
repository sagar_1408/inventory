<?php 
	include("config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Stock Industry an Industrial Category Bootstrap responsive Website Template | Codes :: w3layouts</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
  <meta name="keywords" content="Stock Industry Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
  <script
    type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
  <!-- bootstrap-css -->
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <!--// bootstrap-css -->
  <!-- css -->
  <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
  <!--// css -->
  <!-- font-awesome icons -->
  <link href="css/font-awesome.css" rel="stylesheet">
  <!-- //font-awesome icons -->
  <!-- font -->
  <link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
    rel="stylesheet">
  <!-- //font -->
  <script src="js/jquery-2.2.3.min.js"></script>
  <script src="js/bootstrap.js"></script>
</head>
<body>
  <!-- w3-banner -->
  <div class="w3-banner-1 jarallax">

    <div class="w3layouts-header-top">
      <div class="container">
        <div class="w3-header-top-grids">
          <div class="w3-header-top-left">
            <p><i class="fa fa-volume-control-phone" aria-hidden="true"></i> +1 234 567 8901</p>
          </div>
          <div class="w3-header-top-right">
            <div class="agileinfo-social-grids">
              <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                <li><a href="#"><i class="fa fa-vk"></i></a></li>
              </ul>
            </div>
            <div class="clearfix"> </div>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
    </div>
    <!-- Menu file -->
    <?php
      include("menu.php");
    ?>
    <!-- Menu file -->
  </div>
  <!-- //w3-banner -->
  <div class="typo">
    <div class="container">
      <div class="page-header">
        <h3 class="bars">Catergory Master</h3>
      </div>
      <div class="col-md-11"></div>
      <div class="col-md-1">
        <button class="btn btn-sm">
          <a href="add_category.php">
            Add Category
          </a>
        </button>
      </div>
      <div class="bs-docs-example">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Sr. No.</th>
              <th>Category Name</th>
              <th>Action</th>
            </tr>
          </thead>
            <?php
              $sql = "SELECT * FROM category_master";
              $result = mysqli_query($conn, $sql);
              if (mysqli_num_rows($result) > 0) {
            ?>
          <tbody>
            <?php
              while($row = mysqli_fetch_assoc($result)) {
                echo "<tr>";
                echo "<td>". $row["category_id"]."</td>";
                echo "<td>". $row["category_name"]."</td>";
                echo "<td>". 
                  "<button class='btn btn-sm btn-info'>
                  <i class='fa fa-edit' aria-hidden='true'></i>
                  </button>
                  <button class='btn btn-sm btn-danger'>
                  <i class='fa fa-trash' aria-hidden='true'></i>
                  </button>"
                ."</td>";
              }
            } else {
              echo "0 results";
            }
            mysqli_close($conn);
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- footer -->
  <?php
    include("footer.php");
  ?>
  <!-- //footer -->
</body>
</html>