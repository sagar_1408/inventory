<div class="head">
  <div class="container">
    <div class="navbar-top">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
          data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="navbar-brand logo ">
          <h1><a href="index.php">Stock Industry</a></h1>
        </div>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav link-effect-4">
          <li class="active first-list"><a href="index.php">Home</a></li>
          <li><a href="about.php">About</a></li>
          <li><a href="gallery.php">Gallery</a></li>
          <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
              aria-expanded="false"><span data-letters="Pages">Masters</span><span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="product_master.php">Product Master</a></li>
              <li><a href="brand_master.php">Brand Master</a></li>
              <li><a href="category_master.php">Category Master</a></li>
              <li><a href="customer_master.php">Customer Master</a></li>
              <li><a href="user_master.php">User Master</a></li>
            </ul>
          </li>
          <li><a href="contact.php">Contact</a></li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div>
  </div>
</div>