<?php 
	include("config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Stock Industry an Industrial Category Bootstrap responsive Website Template | About :: w3layouts</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
  <meta name="keywords" content="Stock Industry Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
  <script
    type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
  <!-- bootstrap-css -->
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <!--// bootstrap-css -->
  <!-- css -->
  <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
  <!--// css -->
  <!-- font-awesome icons -->
  <link href="css/font-awesome.css" rel="stylesheet">
  <!-- //font-awesome icons -->
  <!-- font -->
  <link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
    rel="stylesheet">
  <!-- //font -->
  <script src="js/jquery-2.2.3.min.js"></script>
  <script src="js/bootstrap.js"></script>
</head>
<body>
  <!-- w3-banner -->
  <div class="w3-banner-1 jarallax">
    <div class="w3layouts-header-top">
      <div class="container">
        <div class="w3-header-top-grids">
          <div class="w3-header-top-left">
            <p><i class="fa fa-volume-control-phone" aria-hidden="true"></i> +1 234 567 8901</p>
          </div>
          <div class="w3-header-top-right">
            <div class="agileinfo-social-grids">
              <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                <li><a href="#"><i class="fa fa-vk"></i></a></li>
              </ul>
            </div>
            <div class="clearfix"> </div>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
    </div>
    <!-- Menu file -->
    <?php
      include("menu.php");
    ?>
    <!-- Menu file -->
  </div>
  <!-- //w3-banner -->
  <!-- about -->
  <!-- courses -->
  <div class="courses">
    <div class="container">
      <h2 class="agileits-title">About</h2>
      <div class="agileits_w3layouts_team_grids w3ls_courses_grids">
        <div class="col-md-6 w3ls_banner_bottom_left w3ls_courses_left">
          <div class="w3ls_courses_left_grids">
            <div class="w3ls_courses_left_grid">
              <h3><i class="fa fa-pencil-square-o" aria-hidden="true"></i>What is inventory ?</h3>
              <p>A stock of items held to meet future demand.</p>
              <p>Inventory is list for goods and materials, or those goods and materials themselves, held available in
                stock by a business.</p>
            </div>
            <div class="w3ls_courses_left_grid">
              <h3><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Expand business on social share</h3>
              <p>By keeping updating and maintaining stocks, dealers can also able to share the upcoming stocks news to
                customers throght social medias like
                whatsapp, facebook, instagram etc. for expanding their business.
              </p>
            </div>

          </div>
        </div>
        <div class="col-md-6 agileits_courses_right">
          <img src="images/about.jpg" alt=" " class="img-responsive">
        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>
  <!-- //courses -->
  <!-- features -->
  <div class="features jarallax">
    <div class="container">
      <div class="agileits-heading features-heading">
        <h3 class="agileits-title">Best Services</h3>
      </div>
      <div class="w3_agileits_features_grids">
        <div class="col-md-4 w3_agileits_features_grid">
          <div class="agileits_w3layouts_features_grid">
            <div class="col-xs-4 agileits_w3layouts_features_gridl">
              <div class="agile_feature_grid">
                <i class="fa fa-bullhorn" aria-hidden="true"></i>
              </div>
            </div>
            <div class="col-xs-8 agileits_w3layouts_features_gridr">
              <h4>bibendum ex Nam</h4>
              <p>Sit amet tellus finibus quis dolor sed dignissim.</p>
            </div>
            <div class="clearfix"> </div>
          </div>
        </div>
        <div class="col-md-4 w3_agileits_features_grid">
          <div class="agileits_w3layouts_features_grid">
            <div class="col-xs-4 agileits_w3layouts_features_gridl">
              <div class="agile_feature_grid">
                <i class="fa fa-cog" aria-hidden="true"></i>
              </div>
            </div>
            <div class="col-xs-8 agileits_w3layouts_features_gridr">
              <h4>ac hendrerit ante</h4>
              <p>Sit amet tellus finibus quis dolor sed dignissim.</p>
            </div>
            <div class="clearfix"> </div>
          </div>
        </div>
        <div class="col-md-4 w3_agileits_features_grid">
          <div class="agileits_w3layouts_features_grid">
            <div class="col-xs-4 agileits_w3layouts_features_gridl">
              <div class="agile_feature_grid">
                <i class="fa fa-globe" aria-hidden="true"></i>
              </div>
            </div>
            <div class="col-xs-8 agileits_w3layouts_features_gridr">
              <h4>congue est sapien</h4>
              <p>Sit amet tellus finibus quis dolor sed dignissim.</p>
            </div>
            <div class="clearfix"> </div>
          </div>
        </div>
        <div class="col-md-4 w3_agileits_features_grid">
          <div class="agileits_w3layouts_features_grid">
            <div class="col-xs-4 agileits_w3layouts_features_gridl">
              <div class="agile_feature_grid">
                <i class="fa fa-circle-o-notch" aria-hidden="true"></i>
              </div>
            </div>
            <div class="col-xs-8 agileits_w3layouts_features_gridr">
              <h4>quis auctor dolor</h4>
              <p>Sit amet tellus finibus quis dolor sed dignissim.</p>
            </div>
            <div class="clearfix"> </div>
          </div>
        </div>
        <div class="col-md-4 w3_agileits_features_grid">
          <div class="agileits_w3layouts_features_grid">
            <div class="col-xs-4 agileits_w3layouts_features_gridl">
              <div class="agile_feature_grid">
                <i class="fa fa-share-alt" aria-hidden="true"></i>
              </div>
            </div>
            <div class="col-xs-8 agileits_w3layouts_features_gridr">
              <h4>lacus eget ultri</h4>
              <p>Sit amet tellus finibus quis dolor sed dignissim.</p>
            </div>
            <div class="clearfix"> </div>
          </div>
        </div>
        <div class="col-md-4 w3_agileits_features_grid">
          <div class="agileits_w3layouts_features_grid">
            <div class="col-xs-4 agileits_w3layouts_features_gridl">
              <div class="agile_feature_grid">
                <i class="fa fa-laptop" aria-hidden="true"></i>
              </div>
            </div>
            <div class="col-xs-8 agileits_w3layouts_features_gridr">
              <h4>magna quis rhon</h4>
              <p>Sit amet tellus finibus quis dolor sed dignissim.</p>
            </div>
            <div class="clearfix"> </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>
  <!-- //features -->
  <!-- team -->
  <div class="team">
    <div class="container">
      <h3 class="agileits-title">Our Team</h3>
      <div class="agileinfo-team-grids">
        <div class="col-md-3 wthree-team-grid">
          <img src="images/t1.jpg" alt="">
          <div class="wthree-team-grid-info">
            <h4>Mary Jane</h4>
            <p>Vestibulum</p>
            <div class="team-social-grids">
              <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3 wthree-team-grid">
          <img src="images/t2.jpg" alt="">
          <div class="wthree-team-grid-info">
            <h4>Peter Parke</h4>
            <p>Vestibulum</p>
            <div class="team-social-grids">
              <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3 wthree-team-grid">
          <img src="images/t3.jpg" alt="">
          <div class="wthree-team-grid-info">
            <h4>Jennifer Watson</h4>
            <p>Vestibulum</p>
            <div class="team-social-grids">
              <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3 wthree-team-grid">
          <img src="images/t4.jpg" alt="">
          <div class="wthree-team-grid-info">
            <h4>Steven Wilson</h4>
            <p>Vestibulum</p>
            <div class="team-social-grids">
              <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>
  <!-- //team -->
  <!-- //about -->
  <!-- footer -->
  <?php
    include("footer.php");
  ?>
  <!-- //footer -->
  <script src="js/jarallax.js"></script>
  <script src="js/SmoothScroll.min.js"></script>
  <script type="text/javascript">
    /* init Jarallax */
    $('.jarallax').jarallax({
      speed: 0.5,
      imgWidth: 1366,
      imgHeight: 768
    })
  </script>
</body>
</html>