<?php 
	include("config.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Stock Industry an Industrial Category Bootstrap responsive Website Template | Contact :: w3layouts</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
  <meta name="keywords" content="Stock Industry Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
  <script
    type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
  <!-- bootstrap-css -->
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <!--// bootstrap-css -->
  <!-- css -->
  <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
  <!--// css -->
  <!-- font-awesome icons -->
  <link href="css/font-awesome.css" rel="stylesheet">
  <!-- //font-awesome icons -->
  <!-- font -->
  <link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
    rel="stylesheet">
  <!-- //font -->
  <script src="js/jquery-2.2.3.min.js"></script>
  <script src="js/bootstrap.js"></script>
</head>

<body>
  <!-- w3-banner -->
  <div class="w3-banner-1 jarallax">
    <div class="w3layouts-header-top">
      <div class="container">
        <div class="w3-header-top-grids">
          <div class="w3-header-top-left">
            <p><i class="fa fa-volume-control-phone" aria-hidden="true"></i> +1 234 567 8901</p>
          </div>
          <div class="w3-header-top-right">
            <div class="agileinfo-social-grids">
              <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                <li><a href="#"><i class="fa fa-vk"></i></a></li>
              </ul>
            </div>
            <div class="clearfix"> </div>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
    </div>
    <!-- Menu file -->
    <?php
      include("menu.php");
    ?>
    <!-- Menu file -->
  </div>
  </div>
  <!-- //w3-banner -->
  <!-- contact -->
  <div class="contact agileits">
    <div class="container">
      <h2 class="agileits-title">Contact Us</h2>
      <div class="contact-agileinfo">
        <div class="col-md-7 contact-form wthree">
          <form action="#" method="post">
            <input type="text" name="Name" placeholder="Name" required="">
            <input class="email" type="email" name="Email" placeholder="Email" required="">
            <textarea placeholder="Message" name="Message" required=""></textarea>
            <input type="submit" value="SUBMIT">
          </form>
        </div>
        <div class="col-md-4 contact-right wthree">
          <div class="contact-text w3-agileits">
            <h4>GET IN TOUCH :</h4>
            <p><i class="fa fa-map-marker"></i> Broome St, NY 10002, Canada. </p>
            <p><i class="fa fa-phone"></i> Telephone : +00 111 222 3333</p>
            <p><i class="fa fa-fax"></i> FAX : +1 888 888 4444</p>
            <p><i class="fa fa-envelope-o"></i> Email : <a href="mailto:example@mail.com">mail@example.com</a></p>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>
  <!-- //contact -->
  <!-- map -->
  <div class="map w3layouts">
    <iframe
      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3023.9503398796587!2d-73.9940307!3d40.719109700000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25a27e2f24131%3A0x64ffc98d24069f02!2sCANADA!5e0!3m2!1sen!2sin!4v1441710758555"></iframe>
  </div>
  <!-- //map -->
  <!-- footer -->
  <?php
    include("footer.php");
  ?>
  <!-- //footer -->
</body>

</html>