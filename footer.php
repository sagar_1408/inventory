<div class="agileits-w3layouts-footer">
  <div class="container">
    <div class="col-md-4 w3-agile-grid">
      <h5>About Us</h5>
      <p>Adite Technologies, Provides IT Services Like, Web Development, Android App Development, I-OS App Development, E commerce Services,Graphics Design.
Work with once and get Always In-Support whenever.will provide you well organized and commended code makes it easy for you to customization in Future needs.</p>
      <div class="footer-agileinfo-social">
        <ul>
          <li><a href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#"><i class="fa fa-rss"></i></a></li>
          <li><a href="#"><i class="fa fa-vk"></i></a></li>
        </ul>
      </div>
    </div>
    <div class="col-md-4 w3-agile-grid">
      <h5>Recent Posts</h5>
      <div class="w3ls-post-grids">
        <div class="w3ls-post-grid">
          <div class="w3ls-post-img">
            <a href="#"><img src="images/g1.jpg" alt="" /></a>
          </div>
          <div class="w3ls-post-info">
            <h6><a href="#" data-toggle="modal" data-target="#myModal">Donec vel sapien in erat</a></h6>
            <p>Sept 24,2017</p>
          </div>
          <div class="clearfix"> </div>
        </div>
        <div class="w3ls-post-grid">
          <div class="w3ls-post-img">
            <a href="#"><img src="images/g4.jpg" alt="" /></a>
          </div>
          <div class="w3ls-post-info">
            <h6><a href="#" data-toggle="modal" data-target="#myModal">Donec vel sapien in erat</a></h6>
            <p>Oct 25,2017</p>
          </div>
          <div class="clearfix"> </div>
        </div>
        <div class="w3ls-post-grid">
          <div class="w3ls-post-img">
            <a href="#"><img src="images/g6.jpg" alt="" /></a>
          </div>
          <div class="w3ls-post-info">
            <h6><a href="#" data-toggle="modal" data-target="#myModal">Donec vel sapien in erat</a></h6>
            <p>Nov 06,2017</p>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
    </div>
    <div class="col-md-4 w3-agile-grid">
      <h5>Address</h5>
      <div class="w3-address">
        <div class="w3-address-grid">
          <div class="w3-address-left">
            <i class="fa fa-phone" aria-hidden="true"></i>
          </div>
          <div class="w3-address-right">
            <h6>Phone Number</h6>
            <p>+91 8141008800</p>
          </div>
          <div class="clearfix"> </div>
        </div>
        <div class="w3-address-grid">
          <div class="w3-address-left">
            <i class="fa fa-envelope" aria-hidden="true"></i>
          </div>
          <div class="w3-address-right">
            <h6>Email Address</h6>
            <p>Email :<a href="mailto:info@aditetech.in"> info@aditetech.in</a></p>
          </div>
          <div class="clearfix"> </div>
        </div>
        <div class="w3-address-grid">
          <div class="w3-address-left">
            <i class="fa fa-map-marker" aria-hidden="true"></i>
          </div>
          <div class="w3-address-right">
            <h6>Location</h6>
            <p>108, Sterling Complex,Street of Balaji Showroom, b/h Ashtavinayk Complex, Kalanala Rd, Nanbhawadi, Bhavnagar, Gujarat 364002
              <span>Telephone : +91 8141008800</span>
            </p>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
    </div>
    <div class="clearfix"> </div>
  </div>
</div>
<div class="copyright">
  <div class="container">
    <p>© 2019 Peripheral Inventory. All rights reserved | Design by <a href="http://aditetech.in/">Adite Technologies</a></p>
  </div>
</div>